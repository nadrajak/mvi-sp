# mvi-sp

Repository for the NI-MVI 23/24 semestral project.

## Assignment – ArXiv Recommender 
_For the dataset containing metadata from the ArXiv webpage create an efficient clustering model. Make a review of current state-of-the-art clustering algorithms for text and choose an appropriate one – maybe sentence-BERT. Take inspiration from the NeurIPS Anthology Visualization._ [[Kaggle dataset](https://www.kaggle.com/Cornell-University/arxiv)]

## Implementation
The implementation uploaded to this repository consists of 2 executed Jupyter notebooks exported from Kaggle.
- `mvi-sp-main.ipynb` containing the code used to write the report.
- `mvi-sp-extra.ipynb` containing a manual fine-tuning loop using PyTorch and Transformers to load the pre-trained model.

The original notebooks can be accessed and copied on Kaggle. [[main](https://www.kaggle.com/code/nadrajak/mvi-sp-main)], [[extra](https://www.kaggle.com/code/nadrajak/mvi-sp-extra)]
